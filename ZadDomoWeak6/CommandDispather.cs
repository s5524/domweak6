﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using CalculatorRedistributables;

namespace ZadDomoWeak6
{
    internal class CommandDispather
    {
        
       private List<ICalculation> _calculationList = new List<ICalculation>();

        public CommandDispather()
        {
            AddCalculationsFromPlugins();
            AddNativeCalculations();
        }

        private void AddCalculationsFromPlugins()
        {
            var dllfileNames = GetDllFileNames();
            var dllFiles = GetLoadedAssembles(dllfileNames);
            var plugins = GetPlugedOperations<ICalculation>(dllFiles);
            _calculationList.AddRange(plugins);
        }

        private static List<T> GetPlugedOperations<T>(IEnumerable<Assembly> assemblies)
        {
            var pluginType = typeof(T);
            var pluginCalculators = new List<T>();

            foreach (var assembly in assemblies)
            {
                if (assembly == null)
                {
                    continue;
                }
                var types = assembly.GetTypes();
                var plugimTypes = types.Where(type => !type.IsInterface && !type.IsAbstract &&
                                                      type.GetInterface(pluginType.Name) != null);

                foreach (var type in plugimTypes)
                {
                    pluginCalculators.Add((T)Activator.CreateInstance(type));
                }
            }
            return pluginCalculators;
        }

        private IEnumerable<Assembly> GetLoadedAssembles(IEnumerable<string> dllFileName)
        {
            var assemblies = new List<Assembly>();
            foreach (var dllFile in dllFileName)
            {
                var assemblyName = AssemblyName.GetAssemblyName(dllFile);
                var assembly = Assembly.Load(assemblyName);

                assemblies.Add(assembly);

            }
            return assemblies;
        }

        private IEnumerable<string> GetDllFileNames()
        {
            const string path = @".\Plugins";
            var dllFileNames = new string[] { };
            if (Directory.Exists(path))
            {
                dllFileNames = Directory.GetFiles(path, "*.dll");
            }
            
            return dllFileNames;
        }

        private void AddNativeCalculations()
        {
            _calculationList.Add(new Sum());

        }


        public IEnumerable<string> GetCalculationsNames()
        {
            return _calculationList.Select(x => x.Name);
        }

        public IEnumerable<Income> DoCalculation(string calculationName, IEnumerable<Income> list)
        {

            var calculation = _calculationList.SingleOrDefault(x => x.Name == calculationName);
            return calculation.Calculate(list);
        }

        

    }
}