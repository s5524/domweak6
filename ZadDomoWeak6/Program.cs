﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ZadDomoWeak6
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var listOdCompanies = new CollectData().GetListoOfCompanies();
                var comandsDispather = new CommandDispather();

                foreach (var names in comandsDispather.GetCalculationsNames())
                {
                    Console.WriteLine(names);
                }

                Console.WriteLine("wybierz metode: ");

                int counter = 0;

                var chose = Console.ReadLine();
                foreach (var name in comandsDispather.GetCalculationsNames())
                {
                    if (name == chose)
                    {
                        var companyList = comandsDispather.DoCalculation(chose, listOdCompanies);
                        PrintAllDataCompanies(companyList);
                        counter++;
                    }
                }

                if (counter==0)
                {
                       Console.WriteLine("Brak komędy w bazie");
                }
            }
        }


        private static void PrintAllDataCompanies(IEnumerable<Income> companyList)
        {
            if (!companyList.Any())
            {
                Console.WriteLine("Brak danych dla wprowadzonych informacji");
            }

            foreach (var company in companyList)
            {
                Console.WriteLine("Nazwa :"+company.Name+" wartość: "+company.Value);
            }
        }
    }
}
