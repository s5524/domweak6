﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ZadDomoWeak6
{
    class CollectData
    {
        //private readonly List<Income> _globalIncome = new List<Income>();
        //private int NumberOfCores = 4 ;
        //private static object LockObj = new object();

        //internal List<Income> Run()
        //{
        //    var file = GetListoOfCompanies();
        //    var listOfThreads = new Thread[NumberOfCores];
        //    var partOfList = file.ToArray().Length / NumberOfCores;
        //    var moduloRange = partOfList % NumberOfCores;

        //    for (int i = 0; i < NumberOfCores; i++)
        //    {
        //        var chunk = file.GetRange(partOfList * i + moduloRange, partOfList);

        //        var t = new Thread(() => CountList(chunk));
        //        t.Name = "Thread_" + i;
        //        t.Start();
        //        listOfThreads[i] = t;
        //        moduloRange = 0;
        //    }
        //    foreach (var t in listOfThreads)
        //    {
        //        t.Join();
        //    }

        //    //printAllGlobalIncome();
        //        return file;
        //}

        internal List<Income> GetListoOfCompanies()
        {
            int chose = 33;
            string companyName = "";
            DateTime startingDate = DateTime.Parse("1970-01-01");
            DateTime enddingDate = DateTime.Parse("1970-01-01");
            while (chose!=1 && chose!=2)
            {
                Console.WriteLine("1:Wyswietl wszystkie wyniki do dnia dzisiejszego włącznie\n2:Wprowadz zakres danych do pobrania ");
                try
                {
                    chose = int.Parse(Console.ReadLine());

                }
                catch (Exception e)
                {
                    Console.WriteLine("Zły format");
                }
            }
            if (chose == 2)
            {
                Console.WriteLine("Podaj nazwę firmy");
                companyName = Console.ReadLine();
                startingDate = GetStartingDateFromUser("Podaj date początkową\n");
                enddingDate = GetEndingDateFromUser("Podaj date końcową\n", startingDate);
            }
            if (chose == 1)
            {
                startingDate = DateTime.Parse("1970.01.01");
                enddingDate = DateTime.Now;
                companyName = "";
            }
            string stringFile;
            var file = new List<Income>();


            for (int i = startingDate.Year; i <= enddingDate.Year; i++)
            {
                try
                {


                    stringFile = File.ReadAllText("CompaniesRevenies_" + i + ".json");
                    var list = JsonConvert.DeserializeObject<Income[]>(stringFile);
                    foreach (var log in list)
                    {
                        if (log.Date.Date <= enddingDate.Date && log.Date.Date >= startingDate.Date)
                        {
                            if (companyName != "")
                            {
                                if (log.Name == companyName)
                                {
                                    file.Add(log);
                                }
                            }
                            else
                            {
                                file.Add(log);
                            }
                        }
                    }
                }
            
            catch (Exception e)
            {
                Console.WriteLine("Brak pliku z datą "+startingDate.Year);
            }

        }
            return file;
        }


        private DateTime GetStartingDateFromUser(string text)
        {
            DateTime tempdate;
            Console.Write(text);
            while (true)
            {
                var temp = "";
                while (!DateTime.TryParse(Console.ReadLine(), out tempdate))
                {
                    Console.WriteLine("Not an DateTime - try again...");
                }
                try
                {
                    temp = File.ReadAllText("CompaniesRevenies_" + tempdate.Year + ".json");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Brak pliku z datą w bazie");
                }
                if (temp != "")
                {
                    return tempdate;
                }


            }
        }
        private DateTime GetEndingDateFromUser(string text, DateTime date)
        {
            DateTime tempdate;
            Console.Write(text);
            while (true)
            {
                while (!DateTime.TryParse(Console.ReadLine(), out tempdate))
                {
                    Console.WriteLine("Not an DateTime - try again...");
                }
                if (date > tempdate)
                {
                    Console.WriteLine("data mniejsz od początkowej");
                }
                else
                {
                    var temp = "";
                    try
                    {
                        temp = File.ReadAllText("CompaniesRevenies_" + tempdate.Year + ".json");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Brak pliku z datą w bazie");
                    }
                    if (temp != "")
                    {
                        return tempdate;
                    }
                }

            }

            return tempdate;
        }

        //private void CountList(List<Income> chunk)
        //{


        //    var incomeList = new List<Income>();
        //    foreach (var income in chunk)
        //    {
        //        var temp = incomeList.SingleOrDefault(
        //            d => d.Name == income.Name);
        //        if (temp != null)
        //        {
        //            temp.Value += income.Value;
        //        }
        //        else
        //        {
        //            incomeList.Add(income);
        //        }

        //    }
        //    ;
        //    ToglobalIncomeList(incomeList);
        //}

        //private void ToglobalIncomeList(List<Income> incomeList)
        //{
        //    lock (LockObj)
        //    {
        //        foreach (var income in incomeList)
        //        {
        //            var temp = _globalIncome.SingleOrDefault(
        //                d => d.Name == income.Name);

        //            if (temp != null)
        //            {
        //                temp.Value += income.Value;
        //            }
        //            else
        //            {
        //                _globalIncome.Add(income);
        //            }
        //        }
        //    }

        //}

        //private void printAllGlobalIncome(string companyName)
        //{
        //    if (companyName != "")
        //    {
        //        foreach (var income in _globalIncome)
        //        {
        //            if (income.Name == companyName)
        //                Console.WriteLine(income.Value + " " + income.Name);
        //        }
        //    }
        //    else
        //        foreach (var income in _globalIncome)
        //        {
        //            Console.WriteLine(income.Value + " " + income.Name);
        //        }
        //}
    }
}
