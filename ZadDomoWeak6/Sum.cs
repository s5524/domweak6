﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorRedistributables;
using ZadDomoWeak6;

namespace ZadDomoWeak6
{
    public class Sum : ICalculation
    {
        public string Name => "Sum";
        public IEnumerable<Income> Calculate(IEnumerable<Income> list)
        {
            return CalculateSum(list);
        }

        private IEnumerable<Income> CalculateSum(IEnumerable<Income> list)
        {
            var incomeList = new List<Income>();
            foreach (var income in list)
            {
                var temp = incomeList.SingleOrDefault(
                    d => d.Name == income.Name);
                if (temp != null)
                {
                    temp.Value += income.Value;
                }
                else
                {
                    incomeList.Add(income);
                }
            }
            
            return incomeList;
        }
    }
}
