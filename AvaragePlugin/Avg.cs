﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorRedistributables;
using ZadDomoWeak6;

namespace AvaragePlugin
{
    public class Avg :ICalculation
    {
        public string Name => "Avg";
        public IEnumerable<Income> Calculate(IEnumerable<Income> list)
        {
            return CalculateAvarage(list);
        }

        private IEnumerable<Income> CalculateAvarage(IEnumerable<Income> list)
        {
            var incomeList = new List<Income>();
            
            var result = list.GroupBy(s => s.Name, s => s.Value,
                (companyName, value) => new {name = companyName, Avg = value.Average()});
            foreach (var log in result)
            {
                var income = new Income();
                income.Name = log.name;
                income.Value = (long)log.Avg;
                incomeList.Add(income);
            }


            return incomeList;
        }
    }
}
