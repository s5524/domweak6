﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZadDomoWeak6;

namespace CalculatorRedistributables
{
    public interface ICalculation
    {
        string Name { get; }
        IEnumerable<Income> Calculate(IEnumerable<Income> list);
    }
}
